package mechanics

import "testing"

func TestRSplitSlice(t *testing.T) {
	for _, tt := range []struct {
		s          []int
		directions []Side
		want       int
	}{
		{
			s:          []int{1, 2},
			directions: []Side{Left},
			want:       1,
		},
		{
			s:          []int{1},
			directions: []Side{Right},
			want:       1,
		},
		{
			s:          []int{1},
			directions: []Side{Left},
			want:       1,
		},
		{
			s:          []int{1, 2, 3},
			directions: []Side{Left},
			want:       1,
		},
		{
			s:          []int{1, 2, 3, 4},
			directions: []Side{Left, Right},
			want:       2,
		},
		{
			s:          []int{1, 2, 3, 4},
			directions: []Side{Left, Left},
			want:       1,
		},
		{
			s:          []int{1, 2, 3, 4},
			directions: []Side{Right, Right},
			want:       4,
		},
		{
			s:          []int{1, 2, 3, 4},
			directions: []Side{Right, Left},
			want:       3,
		},
	} {
		i := 0

		// nextSide will iterate through all the directions. Once all
		// directions are exhausted, it will fail the test if accessed again.
		nextSide := func() Side {
			defer func() { i++ }()

			if i >= len(tt.directions) {
				t.Fatalf("no more directions after %d steps",
					len(tt.directions)-1)
			}

			ns := tt.directions[i]
			t.Logf("Yielding next direction: %s", ns)

			return ns
		}

		actual := RSplitSlice(tt.s, nextSide)
		t.Logf("Input slice %v with directions %v returned %d",
			tt.s, tt.directions, actual)

		if actual != tt.want {
			t.Errorf("Expected %d but got %d", tt.want, actual)
		}
	}
}
